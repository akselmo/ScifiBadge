# ScifiBadge

Simple scifi-y badge that shows my info. Made for [Tufty2040](https://pimoroni.com/Tufty2040)

![Preview of the badge](./preview.png)