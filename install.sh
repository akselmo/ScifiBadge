#!/usr/bin/env bash
file=$1
rpi=$2

echo "Waiting for RPI-RP2 to be mounted"
until [ -d $rpi ]
do
    sleep 5
done
echo "Copying $file to $rpi"
cp $file $rpi
echo "Done!"