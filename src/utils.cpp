#include "utils.hpp"

uint32_t Utils::Time() {
    absolute_time_t t = get_absolute_time();
    return to_ms_since_boot(t);
}
