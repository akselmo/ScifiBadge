#include "screen.hpp"

using namespace pimoroni;

int Screen::AutoBrightness(double old_brightness)
{
    // Enable power
    gpio_put(LUX_PWR_PIN, 1);

    // Get value
    auto lux = adc_read();

    // Calculate brightness
    double lux_normalized = ((double)lux - LUX_LOW) / (LUX_HIGH - LUX_LOW);
    auto new_brightness = BRIGHTNESS_HIGH * lux_normalized;
    auto brightness_diff = new_brightness - old_brightness;
    new_brightness = old_brightness + (brightness_diff * (1.0 / 32.0));
    new_brightness = CLAMP(new_brightness, BRIGHTNESS_LOW, BRIGHTNESS_HIGH);

    // Disable power
    gpio_put(LUX_PWR_PIN, 0);

    // Print brightness value
    /*
    char buff[100];
    snprintf(buff, sizeof(buff), "Lux val: %i", (int)new_brightness);
    std::string lux_value = buff;
    Draw::Text(lux_value, Point(127, 127), WHITE, 2.5f);
    */

    // Set backlight and led value and return the new one
    ScifiBadge::st7789.set_backlight(new_brightness);
    ScifiBadge::tufty.led(new_brightness);
    return new_brightness;

}