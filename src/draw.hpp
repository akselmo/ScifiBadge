#pragma once
#include <string>
#include "libraries/pico_graphics/pico_graphics.hpp"
#include "color.hpp"
#include "images.hpp"
#include "strings.hpp"
#include "scifibadge.hpp"

using namespace pimoroni;

class Draw {
public:
    static void Background(Color color);
    static void Box(Point start, Point end, Color color);
    static void Text(std::string text, Point text_location, Color color, float size);
    static void Image(const std::vector<std::vector<Color>> &image, uint32_t start_x, uint32_t start_y);
    static void Info(Color color);
    static void Misc(Color color);
    static void SlantedBox(Color color);
    static void SetColor(Color color);
};