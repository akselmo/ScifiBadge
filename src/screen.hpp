#pragma once
#include "pins.hpp"
#include "hardware/adc.h"
#include "hardware/regs/addressmap.h"
#include "hardware/gpio.h"
#include "hardware/pio.h"
#include "hardware/vreg.h"
#include "pico/stdio.h"
#include "pimoroni_common.hpp"
#include <string>
#include "common/pimoroni_common.hpp"
#include "pico_graphics.hpp"
#include "draw.hpp"
#include "st7789.hpp"
#include "tufty2040.hpp"
#include "scifibadge.hpp"

using namespace pimoroni;

class Screen {

public:
    static int AutoBrightness(double old_brightness);
};
