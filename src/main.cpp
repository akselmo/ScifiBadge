#include "scifibadge.hpp"

// Main loop
int main() {
    ScifiBadge::Initialize();
    while (true)
    {
        ScifiBadge::Run();
    }

    return 0;
}


