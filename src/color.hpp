#pragma once
#include <cstdio>


// This honestly works better as a header file, making it a class adds unnecessary overhead
struct Color
{
    uint8_t Red;
    uint8_t Green;
    uint8_t Blue;
};

const Color WHITE = {
    .Red = 255,
    .Green = 255,
    .Blue = 255
};

const Color BLACK = {
    .Red = 0,
    .Green = 0,
    .Blue = 0
};