#pragma once
#include "cstdio"

// Pin constants
const uint LUX_PWR_PIN = 27;
const uint LUX_PIN = 26;
const uint LUX_ADC = 0;

// Screen brightness constants
const double BRIGHTNESS_LOW = 95.0; // Anything below this turns screen off, at least on my device.
const double BRIGHTNESS_HIGH = 255.0;
const uint LUX_LOW = 150;
const uint LUX_HIGH = 3500;
