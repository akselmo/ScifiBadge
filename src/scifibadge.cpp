#include "scifibadge.hpp"

using namespace pimoroni;

// Globals
uint8_t ScifiBadge::backlight_brightness;
uint8_t ScifiBadge::current_view = 1;

// Tufty board
Tufty2040 ScifiBadge::tufty;

// Screen
ST7789 ScifiBadge::st7789 = ST7789(
    Tufty2040::WIDTH,
    Tufty2040::HEIGHT,
    ROTATE_180,
    ParallelPins{
        Tufty2040::LCD_CS,
        Tufty2040::LCD_DC,
        Tufty2040::LCD_WR,
        Tufty2040::LCD_RD,
        Tufty2040::LCD_D0,
        Tufty2040::BACKLIGHT
    }
);

// Graphics view
PicoGraphics_PenRGB332 ScifiBadge::graphics = PicoGraphics_PenRGB332(ScifiBadge::st7789.width, ScifiBadge::st7789.height, nullptr);

// Buttons
Button ScifiBadge::button_a(tufty.A, Polarity::ACTIVE_HIGH);
Button ScifiBadge::button_b(tufty.B, Polarity::ACTIVE_HIGH);
Button ScifiBadge::button_c(tufty.C, Polarity::ACTIVE_HIGH);
Button ScifiBadge::button_up(tufty.UP, Polarity::ACTIVE_HIGH);
Button ScifiBadge::button_down(tufty.DOWN, Polarity::ACTIVE_HIGH);

// Initialize badge
void ScifiBadge::Initialize() {
    stdio_init_all();

    backlight_brightness = BRIGHTNESS_HIGH;
    st7789.set_backlight(backlight_brightness);

    // Light sensor
    adc_init();
    adc_gpio_init(LUX_PIN);
    adc_select_input(LUX_ADC);
    gpio_init(LUX_PWR_PIN);
    gpio_set_dir(LUX_PWR_PIN, true);
}

// Run badge program
int ScifiBadge::Run()
{

    graphics.clear();
    Draw::Background(BLACK);

    ReadViewButton();
    DrawView();

    // Handle backlight brightness
    backlight_brightness = Screen::AutoBrightness(backlight_brightness);

    st7789.update(&graphics);

    return 0;
}

void ScifiBadge::ReadViewButton()
{
    if (button_a.read())
    {
        current_view = 1;
    }
    else if (button_b.read())
    {
        current_view = 2;
    }
    else if (button_c.read())
    {
        current_view = 3;
    }

}

void ScifiBadge::DrawView()
{
    switch (current_view)
    {
        case 1:
            View_One();
            break;

        case 2:
            View_Two();
            break;
        
        case 3:
            View_Three();
            break;
    }

}

// First screen
void ScifiBadge::View_One()
{
    Draw::Box(Point(5,5), Point(103,103), WHITE);
    Draw::Image(Images::Avatar, 6, 6);
    Draw::Info(WHITE);
    Draw::Misc(WHITE);
    Draw::SlantedBox(WHITE);
}

// Second screen
void ScifiBadge::View_Two()
{
    Draw::Text("2!", Point(5,5), WHITE, 3.0f);
}


// Third screen
void ScifiBadge::View_Three()
{
    Draw::Text("3!", Point(5,5), WHITE, 3.0f);
}
