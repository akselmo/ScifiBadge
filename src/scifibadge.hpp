#pragma once
#include "button.hpp"
#include "tufty2040.hpp"
#include "st7789.hpp"
#include "draw.hpp"
#include "screen.hpp"
#include "pins.hpp"
#include "color.hpp"

using namespace pimoroni;

class ScifiBadge{

    public:
        static uint8_t backlight_brightness;
        static uint8_t current_view;
        static Tufty2040 tufty;

        static ST7789 st7789;

        static PicoGraphics_PenRGB332 graphics;

        static Button button_a;
        static Button button_b;
        static Button button_c;
        static Button button_up;
        static Button button_down;

        static void Initialize();
        static int Run();

        static void ReadViewButton();
        static void DrawView();

        static void View_One();
        static void View_Two();
        static void View_Three();

};
