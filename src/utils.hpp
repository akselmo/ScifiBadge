#pragma once
#include <cstdio>
#include "pico/time.h"
#include "pico/types.h"

class Utils
{
    public:
        static uint32_t Time();
};
