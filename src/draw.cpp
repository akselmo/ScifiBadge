#include "draw.hpp"

using namespace pimoroni;

void Draw::Background(Color color)
{
    Draw::SetColor(color);
    ScifiBadge::graphics.clear();
}

void Draw::Image(const std::vector<std::vector<Color>> &image, uint32_t start_x, uint32_t start_y)
{
    for (uint32_t y = 0; y < image.capacity(); y++)
    {
        for (uint32_t x = 0; x < image[y].capacity(); x++)
        {
            ScifiBadge::graphics.set_pen(image[y][x].Red, image[y][x].Green, image[y][x].Blue);
            ScifiBadge::graphics.set_pixel(Point(x+start_x, y+start_y));
        }
    }
}

void Draw::Info(Color color)
{
    Draw::SetColor(color);
    Draw::Text(STRING_NAME, Point(112,32-20), color, 2.0f);
    ScifiBadge::graphics.line(Point(112,32), Point(314,32));

    Draw::Text(STRING_CALLSIGN, Point(112,64-20), color, 2.0f);
    ScifiBadge::graphics.line(Point(112,64), Point(314,64));

    Draw::Text(STRING_RACE, Point(112,96-20), color, 2.0f);
    ScifiBadge::graphics.line(Point(112,96), Point(314,96));
}

void Draw::Misc(Color color)
{
    Draw::Box(Point(5,111), Point(104,234), color);
}

void Draw::SlantedBox(Color color)
{
    Draw::SetColor( color);
    ScifiBadge::graphics.line(Point(112,111), Point(314,111));
    ScifiBadge::graphics.line(Point(314,111), Point(314,194));
    ScifiBadge::graphics.line(Point(314,195), Point(274,234));
    ScifiBadge::graphics.line(Point(274,234), Point(112,234));
    ScifiBadge::graphics.line(Point(112,234), Point(112,111));
}

void Draw::Box(Point start, Point end, Color color)
{

    Draw::SetColor(color);
    // Top
    ScifiBadge::graphics.line(Point(start.x,start.y), Point(end.x,start.y));
    // Bottom
    ScifiBadge::graphics.line(Point(start.x,end.y), Point(end.x,end.y));
    // Left
    ScifiBadge::graphics.line(Point(start.x,start.y), Point(start.x,end.y));
    // Right
    ScifiBadge::graphics.line(Point(end.x,start.y), Point(end.x,end.y));

}

void Draw::Text(std::string text, Point text_location, Color color, float size)
{
    Draw::SetColor(color);
    ScifiBadge::graphics.text(text, text_location, 320, size);
}

void Draw::SetColor(Color color)
{
    ScifiBadge::graphics.set_pen(color.Red, color.Green, color.Blue);
}